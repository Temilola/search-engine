<!DOCTYPE html>
<html>
  <head lang="en">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>CTSMaRT™... Search on.</title>

    <!--CSS imports-->
    <link rel="stylesheet" href="{{asset('assets/css/bootstrap.css')}}">
    <link rel="stylesheet" href="{{asset('assets/css/custom.css')}}">
  </head>
  <body>

    <div class="ct-content">

      <nav class="navbar navbar-default">
        <div class="container-fluid">
          <!-- Brand and toggle get grouped for better mobile display -->
          <div class="navbar-header">
            <a class="navbar-brand" href="#">
              <img alt="icon" src="{{asset('assets/images/cts-logo.png')}}" style="width: 3em;">
            </a>
          </div>

          <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <form class="navbar-form navbar-right" role="search" action="{{url('/logout')}}" method="post">
              {!! csrf_field() !!}
              @include('common.errors')
              <button type="submit" class="btn btn-default badge">Log Out</button>
            </form>

          </div><!-- /.navbar-collapse -->
        </div><!-- /.container-fluid -->
      </nav>




      <div class="col-sm-8 col-md-offset-2 text-center">
        <h3>How To Use</h3>
      </div>
      <div class="inner bg">
        <div class="col-sm-2 col-md-offset-1 text-center">
          <h5>
            <strong> Input a keyword</strong>
          </h5>
        </div>

      </div>



  </div>




  </body>
</html>