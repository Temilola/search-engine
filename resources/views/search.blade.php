<!DOCTYPE html>
<html>
    <head lang="en">
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="csrf-token" content="{{ csrf_token() }}" />
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>CTSMaRT™... Search on.</title>

        <!--CSS imports-->
        <link rel="stylesheet" href="{{asset('assets/css/bootstrap.css')}}">
        <link rel="stylesheet" href="{{asset('assets/css/custom.css')}}">
    </head>
    <body>

        <div class="ct-content">
                @if(Session::has('error'))
                    <div class="alert alert-danger">
                        <h2>{{ Session::get('error') }}</h2>
                    </div>
                @endif
                

            <nav class="navbar navbar-default">
              <div class="container-fluid">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                  <a class="navbar-brand" href="#">
                    <img alt="icon" src="{{asset('assets/images/cts-logo.png')}}" style="width: 3em;">
                  </a>
                </div>

                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                  <form class="navbar-form navbar-right" role="search" action="{{url('/logout')}}" method="post">
                  {!! csrf_field() !!}
                    <button type="submit" class="btn btn-default badge">Log Out</button>
                  </form>
                  <a class="navbar-form navbar-right" href="">HELP</a>
                  <!-- <a class="navbar-form navbar-right" href="{!!url('/help')!!}">HELP</a> -->
                  <!-- <a class="navbar-form navbar-right" href="{!!url('/faq')!!}">FAQ</a> -->
                  <a class="navbar-form navbar-right" href="">FAQ</a>

                </div><!-- /.navbar-collapse -->
              </div><!-- /.container-fluid -->
            </nav>

                
                
            <div class="inner-bg">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-8 col-md-offset-2 text-center">
                            <h3>Search Engine</h3>
                            <form role="form" action="{{url('/search_result')}}" method="post">
                            {!! csrf_field() !!}
                                <div class="form-group">
                                    <input name="search_field" type="search" class="form-control glyphicon glyphicon-search" placeholder="Search here...">
                                </div>
                                
                                <div class="col-sm-15">
                                    <!-- <a class="btn btn-primary" href="configureData.html"> Search </a> -->
                                    <button class="btn btn-primary"> Search</button>
                                </div>
                            </form>

                            <!--<div id="ct-search-interface" class="row">-->
                                <!--<div class="col-sm-8">-->
                                    <!--<h5>Advanced option:</h5>-->
                                    <!--<h5><a href="#"> xxxxx</a> | </h5>-->
                                    <!--<h5><a href="#"> xxxxx</a> | </h5>-->
                                    <!--<h5><a href="#"> xxxxx</a> | </h5>-->
                                    <!--<h5><a href="#"> xxxxx</a> | </h5>-->
                                    <!--<h5><a href="#"> xxxxx</a></h5>-->
                                <!--</div>-->
                            <!--</div>-->
                            

                        </div>
                    </div>

                </div>
            </div>
        </div>
    </body>

     <footer class="footerDown">

                    <div class="row text-center">
                            <div class="col-sm-6 text-right">
                                <a class="btn btn-primary" href="{{url('/configureData')}}">Configure Data</a>
                            </div>
                            <div class="col-sm-6 text-left">
                                <a class="btn btn-primary" href="{{url('/monitorData')}}">Monitor Data Sources</a>
                            </div>
                    </div>
                    <div class="centered">
                        
                            <h5>Copyright 2016</h5>
                        
                    </div>
              
     </footer>
</html>