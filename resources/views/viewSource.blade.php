<!DOCTYPE html>
<html>
    <head lang="en">
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>CTSMaRT™... Search on.</title>

        <!--CSS imports-->
        <link rel="stylesheet" href="{{asset('assets/css/bootstrap.css')}}">
        <link rel="stylesheet" href="{{asset('assets/css/custom.css')}}">
    </head>
    <body>

    <div class="ct-content">


            <nav class="navbar navbar-default">
              <div class="container-fluid">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="nav navbar-header">
                  <a class="navbar-brand" href="#">
                    <img alt="icon" src="{{asset('assets/images/cts-logo.png')}}" style="width: 3em;">
                  </a>
                </div>


                 




                <!-- Collect the nav links, forms, and other content for toggling -->
                <!-- <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                  <form class="navbar-form navbar-right" role="search">
                    <div class="form-group">
                      <input type="text" class="form-control" placeholder="Search">
                    </div>
                    <button type="submit" class="btn btn-default">Submit</button>
                  </form>



                  <div>
                      <ul class="nav navbar-nav navbar-right">
                        <li><a href="#">Add User</a></li>
                        <li class="dropdown">
                          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Advance Search<span class="caret"></span></a>
                          <ul class="dropdown-menu">
                            <li><a href="#">Action</a></li>
                            <li><a href="#">Another action</a></li>
                            <li><a href="#">Something else here</a></li>
                            <li role="separator" class="divider"></li>
                            <li><a href="#">Separated link</a></li>
                            <li role="separator" class="divider"></li>
                            <li><a href="#">One more separated link</a></li>
                          </ul>
                        </li>
                      </ul>
                  </div> -->
                  
              </div><!-- /.container-fluid -->
            </nav>


            <table class="table table-bordered table-hover table-striped">
              <thead>
                <th>ID</th>
                <th>DATA TYPE</th>
                <th>PATH</th>
                <th>Action</th>
              </thead>
              <tbody>
                @foreach($data as $source)
                  <tr>
                    <td> {!!nl2br($source['id'])!!}</td>
                    <td> {!!nl2br($source['type'])!!}</td>
                    <td> {!!nl2br($source['path'])!!}</td>
                    <td> <a class="btn btn-default" href="{{url('/monitorData/edit')}}/{{$source['id']}}">EDIT</a></td>
                  </tr>
                @endforeach
              </tbody>
              
            </table>




    </div>

    
    </body>

     <footer class="footerDown">
                  <div class="centered">

                      <a class='btn btn-primary' href="{!!url('/monitorData')!!}"> Back </a>
                      
                  </div>
                
                    <div class="centered">
                        
                            <h5>Copyright 2016</h5>
                        
                    </div>
              
     </footer>
</html>