<!DOCTYPE html>
<html>

    <head lang="en">
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="csrf-token" content="{{ csrf_token() }}" />
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>CTSMaRT™</title>

        <!--CSS imports-->
        <link rel="stylesheet" href="{{asset('assets/css/bootstrap.css')}}">
        <link rel="stylesheet" href="{{asset('assets/css/custom.css')}}">
    </head>
    <body>
        <!--main content-->
                                                @include('common.errors')
        <div class="ct-content">
            <div class="inner-bg">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-4 col-sm-offset-1">
                            <!--the logo-->
                            <div>
                                <img src="{{asset('assets/images/cts-logo.png')}}" class="img img-responsive">
                                <h3>CTSMaRT™ Search Engine</h3>
                            </div>
                        </div>
                        <div class="col-sm-4 col-sm-offset-1">
                            <!--the login section-->
                            <div id="ct-login">
                                <div class="form-top">
                                    <div class="form-top-left">
                                        <h4>Login</h4>
                                    </div>
                                </div>
                                <div class="form-bottom">
                                    <form role="form" action="{{url('/home')}}" method="post">
                                    {!! csrf_field() !!}
                                    <!-- <input type="hidden" name="_token" value="{{ csrf_token() }}"> -->

                                        <div class="form-group">
                                            <input name="username" type="text" title="Username" class="form-control" placeholder="Username">
                                        </div>
                                        <div class="form-group">
                                            <input name="password" type="password" title="Password" class="form-control" placeholder="Password">
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-4">
                                                <button id="submit" class="btn btn-primary">Login</button>
                                            </div>
                                            <div class="col-sm-8 text-right">
                                                <h6><a href="<?php echo url('/forgot_password'); ?>">Forgot your password?</a></h6>
                                            </div>
                                        </div>

                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <!--the footer section-->
           
        </div>
    </body>


     <footer class="footerDown">
                
                    <div class="centered">
                        
                            <h5>Copyright 2016</h5>
                        
                    </div>
              
     </footer>
</html>