<!DOCTYPE html>
<html>
    <head lang="en">
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>CTSMaRT™... Search on.</title>

        <!--CSS imports-->
        <link rel="stylesheet" href="{{asset('assets/css/bootstrap.css')}}">
        <link rel="stylesheet" href="{{asset('assets/css/custom.css')}}">
    </head>
    <body>

    <div class="ct-content">


            <nav class="navbar navbar-default">
              <div class="container-fluid">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="nav navbar-header">
                  <a class="navbar-brand" href="#">
                    <img alt="icon" src="{{asset('assets/images/cts-logo.png')}}" style="width: 3em;">
                  </a>
                </div>


                 




                <!-- Collect the nav links, forms, and other content for toggling -->
                <!-- <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                  <form class="navbar-form navbar-right" role="search">
                    <div class="form-group">
                      <input type="text" class="form-control" placeholder="Search">
                    </div>
                    <button type="submit" class="btn btn-default">Submit</button>
                  </form>



                  <div>
                      <ul class="nav navbar-nav navbar-right">
                        <li><a href="#">Add User</a></li>
                        <li class="dropdown">
                          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Advance Search<span class="caret"></span></a>
                          <ul class="dropdown-menu">
                            <li><a href="#">Action</a></li>
                            <li><a href="#">Another action</a></li>
                            <li><a href="#">Something else here</a></li>
                            <li role="separator" class="divider"></li>
                            <li><a href="#">Separated link</a></li>
                            <li role="separator" class="divider"></li>
                            <li><a href="#">One more separated link</a></li>
                          </ul>
                        </li>
                      </ul>
                  </div> -->
                  
                </div><!-- /.navbar-collapse -->
              </div><!-- /.container-fluid -->
            </nav>






             <div class="inner-bg">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-6 col-sm-offset-3">
                            <!--the logo-->
                          <!--     <div>
                                <img src="images/cts-logo.png" class="img img-responsive">
                                <h3>CTSMaRT™ Search Engine</h3>
                            </div>
                          </div>
                          <div class="col-sm-4 col-sm-offset-1">-->
                            <!--the login section-->
                            <div id="">
                                <div class="form-top">
                                    <div class="form-top-center text-center">
                                        <h4>Enabled Config</h4>
                                    </div>
                                </div>
                                <div class="form-bottom">
                                  <form role="form" action="{{url('/configureData/saveConfig')}}" method="post">
                                    {!! csrf_field() !!}
                                    <div class="form-group row">
                                      <label for="index" class="col-sm-2 form-control-label">Index:</label>    
                                      <div class="col-sm-10">
                                        {!!nl2br($data['index'])!!}
                                      </div>
                                    </div>
                                    <div class="form-group row">
                                      <label for="type" class="col-sm-2 form-control-label">Type:</label>
                                      <div class="col-sm-10">
                                      {!!nl2br($data['type'])!!}
                                      </div>
                                    </div>
                                    <div class="form-group row">
                                      <label for="fields" class="col-sm-2 form-control-label">Fields:</label>
                                      <div class="col-sm-10">
                                        {!!nl2br($data['fields'])!!}
                                      </div>
                                    </div>
                                    <div class="form-group row">
                                      <label for="analysis" class="col-sm-2 form-control-label">Analysis:</label>
                                      <div class="col-sm-10">
                                        {!!nl2br($data['analysis'])!!}
                                      </div>
                                    </div>
                                    <div class="row">
                                      <div class="col-sm-7 text-right">
                                        @include('common.errors')
                                        <a class='btn btn-primary' href="{!!url('/configureData')!!}"> Back </a>
                                      </div>
                                    </div>
                                  </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </div>




    </body>

     <footer class="footerDown">
                
                    <div class="centered">
                        
                            <h5>Copyright 2016</h5>
                        
                    </div>
              
     </footer>
</html>