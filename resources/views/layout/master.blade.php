
<!DOCTYPE html>
<html>
    <head lang="en">
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>CTSMaRT™... Search on.</title>

        <!--CSS imports-->
        <link rel="stylesheet" href="{{asset('assets/css/bootstrap.css')}}">
        <link rel="stylesheet" href="{{asset('assets/css/custom.css')}}">
    </head>
    <body>

    @yield('content')


    </body>

     <div>
        <footer class="footerDown">
                    <div class="row text-center">
                            <div class="col-sm-6 text-right">
                                <a class="btn btn-primary" href="configureData.html">Configure Data Sources</a>
                            </div>
                            <div class="col-sm-6 text-left">
                                <a class="btn btn-primary" href="monitorData.html">Monitor Data Sources</a>
                            </div>
                    </div>

                    <div class="centered">
                        
                            <h5>Copyright 2016</h5>
                        
                    </div>
              
        </footer>
    </div>
</html>