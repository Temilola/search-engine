<!DOCTYPE html>
<html>
    <head lang="en">
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="csrf-token" content="{{ csrf_token() }}" />
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>CTSMaRT™... Search on.</title>

        <!--CSS imports-->
        <link rel="stylesheet" href="{{asset('assets/css/bootstrap.css')}}">
        <link rel="stylesheet" href="{{asset('assets/css/custom.css')}}">
    </head>
    <body>
	    <div class='ct-content'>
		    <div id='ct-navbar' class='navbar navbar-nav'>
		        <img id='ct-logo' src="{{asset('assets/images/cts-logo.png')}}" class='img img-responsive'>
		    </div>

		    <div class='col-sm-8 col-md-offset-2 text-center'>
		    	<br><br>
		    	<form role="form" action="{{url('/search_result')}}" method="post">
                	{!! csrf_field() !!}
			    	<div class="form-group">
			    		<input type="search" name="search_field" class="form-control" value="{!!nl2br($search)!!}">
			    	</div>
				    <div class="col-sm-12">
            				@include('common.errors')
	                        <button class="btn btn-primary">Search</button>
			               	<a class='btn btn-primary' href="{!!url('/search')!!}"> Back </a>
	                </div>
                </form>
                <br>
		    </div>
			<div class='inner-bg'>
			    <div class='container'>
			        <div class='row'>
		    			<div class="col-sm-12">
		    				<p>
		    					<h3>Search Results</h3>
		    				</p>
		    			</div>
						@foreach ($results as $result)
				            <!-- <h5> {!!nl2br($result['_source']['content'])!!} </h5>  -->
				            <?php 
				               	$search = substr($result['_source']['content'], 0, 1000);
				                echo $search.' ...';
				            ?>
				               	<!-- <a href="{{url('search_result')}}/{{$result['_id']}}" class="btn btn-link"> Click to read more</a> -->
				               	<a href="#sample-modal" data-toggle="modal" data-target="#get-a-result-{{$result['_id']}}" class="btn btn-link"> More </a>
				               	<!-- href="#sample-modal" data-toggle="modal" data-target="#add-new-lga"> -->


						<br><br>

						@endforeach
			        </div>
			    </div>
			</div>
			@foreach ($results as $result)
				<div class="modal fade" id="get-a-result-{{$result['_id']}}">
	        		<div class="modal-dialog">
	            		<div class="modal-content">

			                <div class="modal-header">
			                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			                    <h4 class="modal-title bold">Add Local Government</h4>
			                </div>

			                <div class="modal-body">
								Hello I am a Modal!
			                </div>
			                <div class="modal-footer">
								<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
								<button type="button" class="btn btn-info">Save changes</button>
							</div>
			            </div>
			        </div>
			    </div>
		    @endforeach
		</div>
    </body>
</html>