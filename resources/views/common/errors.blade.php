@if(Session::has('error'))
  <div class="alert alert-danger">
    <h2>{{ Session::get('error') }}</h2>
  </div>
@endif

<!-- @if(session('status'))
  <div>
    {{ session('status') }}
  </div>
@endif

@if (session('Update_failed'))
  <div class="mdl-color-text--white bg-red mdl-card">
    {{ session('Update_failed') }} 
  </div>
@endif -->

@if(Session::has('success'))
    <div class="alert-box success">
        <h2>{{ Session::get('success') }}</h2>
    </div>
@endif