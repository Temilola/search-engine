<!DOCTYPE html>
<html>
    <head lang="en">
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="csrf-token" content="{{ csrf_token() }}" />
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>CTSMaRT™... Search on.</title>

        <!--CSS imports-->
        <link rel="stylesheet" href="{{asset('assets/css/bootstrap.css')}}">
        <link rel="stylesheet" href="{{asset('assets/css/custom.css')}}">
    </head>
    <body>

    <div class="ct-content">


            <nav class="navbar navbar-default">
              <div class="container-fluid">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                  <a class="navbar-brand" href="#">
                    <img alt="icon" src="{{asset('assets/images/cts-logo.png')}}" style="width: 4em;">
                  </a>
                </div>


                 




                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                  <form class="navbar-form navbar-right" role="search" action="{{url('/search_result')}}" method="post">
                  {!! csrf_field() !!}
                    <div class="form-group">
                      <input type="text" class="form-control" name="search_field" placeholder="Search">
                    </div>
                    @include('common.errors')
                    <button type="submit" class="btn btn-default">Submit</button>
                  </form>



                    <div>
                        <ul class="nav navbar-nav navbar-right">
                            <li><a href="{{url('/register')}}">Add User</a></li>
                            <li class="dropdown">
                              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Advance Search<span class="caret"></span></a>
                              <ul class="dropdown-menu">
                                <li><a href="#">Action</a></li>
                                <li><a href="#">Another action</a></li>
                                <li><a href="#">Something else here</a></li>
                                <li role="separator" class="divider"></li>
                                <li><a href="#">Separated link</a></li>
                                <li role="separator" class="divider"></li>
                                <li><a href="#">One more separated link</a></li>
                              </ul>
                            </li>
                        </ul>
                    </div>
                  
                </div><!-- /.navbar-collapse -->
              </div><!-- /.container-fluid -->
            </nav>













       <!-- <div id="ct-navbar" class=" navbar navbar-nav">
            <img id="ct-logo" src="images/cts-logo.png" class="img img-responsive">
            <div class="float right">
                 <form role="form" action="" method="post">
                            <div class="form-group">
                                <input name="search-field" type="text" class="form-control" placeholder="Search here...">
                            </div>
                        </form>
            </div>
        </div>-->

        <div class="inner-bg">
            <div class="container">
                <div class="row">
                    <div class="col-sm-8 col-md-offset-2 text-center">
                        <h3>Monitor Data Sources</h3>
                      
                    </div>
                        <!--<div id="ct-search-interface" class="row">-->
                            <!--<div class="col-sm-8">-->
                                <!--<h5>Advanced option:</h5>-->
                                <!--<h5><a href="#"> xxxxx</a> | </h5>-->
                                <!--<h5><a href="#"> xxxxx</a> | </h5>-->
                                <!--<h5><a href="#"> xxxxx</a> | </h5>-->
                                <!--<h5><a href="#"> xxxxx</a> | </h5>-->
                                <!--<h5><a href="#"> xxxxx</a></h5>-->
                            <!--</div>-->
                        <!--</div>-->
                        <div class="row text-center">
                            <div class="col-sm-12 text-center" style="margin-bottom:20px;">
                                <a class="btn btn-primary" href="{{url('/monitorData/view')}}">View Data Source</a>
                            </div>
                            <div class="col-sm-12 ">
                                <a class="btn btn-primary" href="{{url('/monitorData/New')}}">Create A New Data Sources</a>
                            </div>
                        </div>

                    </div>
                </div>

            </div>
        </div>
    </div>




    </body>

    <footer class="footerDown">
                    <div class="centered">

                            <a class='btn btn-primary' href="{!!url('/search')!!}"> Back </a>
                            
                    </div>
                    <div class="centered">
                        
                            <h5>Copyright 2016</h5>
                        
                    </div>
    </footer>
</html>