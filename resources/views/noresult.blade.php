<!DOCTYPE html>
<html>
    <head lang="en">
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="csrf-token" content="{{ csrf_token() }}" />
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>CTSMaRT™... Search on.</title>

        <!--CSS imports-->
        <link rel="stylesheet" href="{{asset('assets/css/bootstrap.css')}}">
        <link rel="stylesheet" href="{{asset('assets/css/custom.css')}}">
    </head>
    <body>
        <div class='ct-content'>
            <div id='ct-navbar' class='navbar navbar-nav'>
                <img id='ct-logo' src="{{asset('assets/images/cts-logo.png')}}" class='img img-responsive'>
            </div>

            <div class='col-sm-8 col-md-offset-2 text-center'>
                <br><br>
                <form role="form" action="{{url('/search_result')}}" method="post">
                    {!! csrf_field() !!}
                    <div class="form-group">
                        <input type="search" name="search_field" class="form-control" value="{!!nl2br($search)!!}">
                    </div>
                    <div class="col-sm-12">
                            @include('common.errors')
                            <button class="btn btn-primary">Search</button>
                            <a class='btn btn-primary' href="{!!url('/search')!!}"> Back </a>
                    </div>
                </form>
                <br><br><br>
            </div>
            <div class='inner-bg'>
                <div class='container'>
                    <div class='row'>
                        <div class="col-sm-12">
                            <center><h3>No Search Result Found</h3></center>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>