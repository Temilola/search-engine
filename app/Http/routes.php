<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });


Route::group(['middleware' => ['web']], function() {

	Route::match(['get','post'],'/logout' ,function () {
        Session::flush();
        return redirect('/');
    });
	
	Route::get('/', function(){
		return view('index');
	});
	Route::post('/home', 'HomeControllers@index');

	Route::get('/register', function(){
		return view('register');
	});
	Route::post('saveDetails', 'HomeControllers@register');

	Route::get('forgot_password', function(){
		return view('forgot_password');
	});

	Route::post('/reset_password','HomeControllers@reset_password');

	Route::get('/search', function(){
		return view('search');
	});

	Route::get('/search_result','HomeControllers@notsearch');
	Route::post('/search_result','HomeControllers@search');

	Route::get('/help','HomeControllers@help');

	Route::get('faq','HomeControllers@faq');


	// Configure Data
	Route::get('/configureData', 'ConfigureDataController@index');

	Route::get('/configureData/New', 'ConfigureDataController@newConfig');

	Route::post('/configureData/saveConfig', 'ConfigureDataController@saveConfig');

	Route::get('/configureData/view', 'ConfigureDataController@viewConfig');

	Route::get('/configureData/view/{key}','ConfigureDataController@viewAConfig');



	// Monitor Data Sources
	Route::get('/monitorData', 'MonitorDataController@index');

	Route::get('/monitorData/New', 'MonitorDataController@newSource');

	Route::post('/monitorData/saveSource', 'MonitorDataController@saveSource');

	Route::get('/monitorData/view', 'MonitorDataController@viewSource');

	Route::get('/monitorData/edit/{id}', 'MonitorDataController@editSource');

	Route::post('/monitorData/edit/{id}', 'MonitorDataController@saveASource');

	Route::get('/monitorData/view/{id}/fetch', 'MonitorDataController@fetchSource');


// Route::post('home', 'HomeControllers@index');

});

// Route::auth();

// Route::get('/home', 'HomeController@index');
