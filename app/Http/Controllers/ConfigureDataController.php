<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use SearchengineAPI;

use Session;

class ConfigureDataController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('configureData');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function newConfig()
    {
        return view('newConfig');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function saveConfig(Request $request)
    {
        $token = Session::get('token');
        // echo $token;
        if (!empty($token)) {
            $fields = $request->fields;
            $analysis = $request->analysis;
            $json_fields = json_encode(explode(',', $fields));
            $json_analysis = json_encode(explode(',', $analysis));
            // echo $json_fields;
            // echo $json_analysis;
            $input = array('index' => $request->index,
                            'type' => $request->type,
                            'fields' => $json_fields,
                            'analysis' => $json_analysis);
            $url = 'http://iqubeglobal.com/ctsmart/api/public/v1/configs?token='.$token;
            $response = $this->searchengineapi()->request('POST', $url, ['form_params' => $input])->getbody();
            // echo $response;
            $responsebody = json_decode($response, true);
            if ($responsebody['status'] == 'ok') {
                return view('configureData')->with('Success','Config successufully created');
            }
            elseif ($responsebody['status'] == 'error'){
                return redirect(url()->previous())->with('Error', $responsebody['error']['msg']);
            }
            else
            {
                return redirect(url()->previous());
            }
        }
        else
        {
            return redirect('/logout')->with('Error','Login Again');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function viewConfig()
    {
        $token = Session::get('token');
        // echo $token;
        if (!empty($token)) {
            $url = 'http://iqubeglobal.com/ctsmart/api/public/v1/configs?token='.$token;
            // echo $url;
            $response = $this->searchengineapi()->request('GET', $url)->getbody();
            // echo $response;
            $responsebody = json_decode($response, true);
            if ($responsebody['status'] == 'ok') {
                return view('viewConfig',['data' => array_get($responsebody, 'data.Configs')]);
            }
            elseif ($responsebody['status'] == 'error') {
                return redirect(url()->previous())->with('Error', $responsebody['error']['msg']);
            }
            else{
                return redirect(url()->previous());
            }
        }
        else
        {
            return redirect('/logout')->with('Error','Login Again');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function viewAConfig($key)
    {
        $token = Session::get('token');
        if (!empty($token)) {
            $response = $this->searchengineapi()->request('GET', "configs/{key}?token={$token}")->getbody();
            echo $response;
        }
        else
        {
            return redirect('/logout')->with('Error','Login Again');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
