<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use SearchengineAPI;

use Session;

class HomeControllers extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $email = $request->username;
        $password = $request->password;
        if (empty($email AND $password)) {
            return redirect(url()->previous())->with('error', 'Fill all Fields');
        }
        else
        {
            $input  = array('email' => $request->username,
                            'password' => $request->password );
            $response = $this->searchengineapi()->request('POST','users/auth',['form_params' => $input])->getbody();
            // echo $response;
            $responsebody = json_decode($response, true);
            if ($responsebody['status'] == 'ok'){
                $token = $responsebody['data']['Token'];
                Session::put('token',$token);

                // $role = $responsebody['data']['']['role'];
                // Session::put('role', $role);
                // $role = Session::get('role');
                // return redirect('search',['role' => $role]);

                // echo $token;
                return redirect('search');

            }
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function register(Request $request)
    {
        $token = Session::get('token');
        if (!empty($token))
        {
            $input = array('name' => $request->fullname,
                            'email' => $request->email,
                            'password' => $request->password,
                            'role' => $request->access);
            $response = $this->searchengineapi()->request('POST',"users?token={$token}",['form_params' => $input])->getbody();
            echo $response;

            $responsebody = json_decode($response, true);
            if ($responsebody['status'] == 'ok'){
                return redirect('/search')->with('success','New User Added');
            }
            elseif ($responsebody['status'] == 'error') {
                return redirect(url()->previous())->with('error', $responsebody['error']['msg']);
            }
            else{
                return redirect(url()->previous());
            }
        }
        else
        {
            return redirect('/logout')->with('Error','Login Again');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request)
    {
        $search = $request->search_field;
        // $url = 'http://iqubeglobal.com/ctsmart/api/public/v1/data/search?query='.$search;
        $response = $this->searchengineapi()->request('GET', "data/search?query={$search}")->getbody();
        // echo $response;
        $responsebody = json_decode($response, true);
        // echo $responsebody['status'];
        $failed = $responsebody['data']['Result']['hits']['total'];
        // echo $failed;
        if ($responsebody['status'] == 'ok') {
            if ($failed == '0') {
                // Session::put('error', 'No Result Found');
                // return redirect(url()->previous())->with('error', 'No Result Found');
                return view('noresult', ['search' => $search]);
            }
            else {
                return view('search_result',['results' => array_get($responsebody, 'data.Result.hits.hits'), 'search' => $search]);
            }
        }
        else if ($responsebody['status'] == 'error') {
            return redirect(url()->previous())->with('error', $responsebody['error']['msg']);
        }
        else
        {
            return redirect(url()->previous());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function help(Request $request)
    {
        return view('help');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function faq()
    {
        return view('faq');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function reset_password(Request $request)
    {
        $email = $request->email;
        $token = Session::get('token');
        $response = $this->searchengineapi()->request('POST','')->getbody();
        $responsebody = json_decode($response, true);
        echo $responsebody;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function notsearch()
    {
        return redirect(url('search'))->with('error', 'This is not allowed');
    }
}
