<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use SearchengineAPI;

use Session;

class MonitorDataController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('monitorData');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function newSource()
    {
        return view('newSource');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function saveSource(Request $request)
    {
        $token = Session::get('token');
        // echo $token;
        if (!empty($token)) {
            $input = array('type' => $request->type,
                            'path' => $request->path,
                            'mapping' => $request->mapping);
            $response = $this->searchengineapi()->request('POST', "sources?token={$token}",['form_params' => $input])->getbody();
            echo $response;
        }
        else
        {
            return redirect('/logout')->with('Error','Login Again');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function viewSource()
    {
        $token = Session::get('token');
        if (!empty($token)) {
            $response = $this->searchengineapi()->request('GET', "sources?token={$token}")->getbody();
            // echo $response;
            $responsebody = json_decode($response, true);
            if ($responsebody['status'] == 'ok') {
                return view('viewSource',['data' => array_get($responsebody, 'data.Sources')]);
            }
            elseif ($responsebody['status'] == 'error') {
                return redirect(url()->previous())->with('Error', $responsebody['error']['msg']);
            }
            else
            {
                return redirect(url()->previous());
            }
        }
        else
        {
            return redirect('/logout')->with('error','Login Again');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function editSource(Request $request, $id)
    {
        $token = Session::get('token');
        if (!empty($token)) {
            $id = $request->id;
            // $url = "http://iqubeglobal.com/ctsmart/api/public/v1/sources/{id}?token={$token}";
            // echo $id;
            $response = $this->searchengineapi()->request('GET', "sources/".$id."?token={$token}")->getbody();
            $responsebody = json_decode($response, true);
            if ($responsebody['status'] == 'ok') {
                return view('editSource', ['data' => array_get($responsebody,'data.Source')]);
            }
        }
        else
        {
            return redirect('/logout')->with('Error','Login Again');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function saveASource(Request $request, $id)
    {
        $token = Session::get('token');
        $id = $request->id;
        echo $id;
        if (!empty($token)) {
            $input = array('mapping' => $request->mapping,
                           'path' => $request->path);
            $response = $this->searchengineapi()->request('POST', "sources/{$id}?token={$token}",['form_params' => $input])->getbody();
            echo $response;
        }
        else
        {
            return redirect('/logout')->with('error','Login Again');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function fetchSource($id)
    {
        $token = Session::get('token');
        if (!empty($token)) {
            $response = $this->searchengineapi()->request('GET', "sources/{id}/fetch?token={$token}")->getbody();
            echo $response;
        }
        else
        {
            return redirect('/logout')->with('Error','Login Again');
        }
    }
}
